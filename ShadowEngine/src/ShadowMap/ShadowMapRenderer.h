//
// Created by dpete on 3/1/2019.
//

#ifndef SHADOWENGINE_SHADOWMAPRENDERER_H
#define SHADOWENGINE_SHADOWMAPRENDERER_H


#include "ShadowMapChunk.h"

class ShadowMapRenderer {

public:
    static void RenderMap(ShadowMapChunk& map);

};


#endif //SHADOWENGINE_SHADOWMAPRENDERER_H
